using Microsoft.AspNetCore.Mvc;
using Persistence;
using HtmlAgilityPack;
using System;
using System.Text;
using Domain;
using Application.Services;

namespace API.Controllers
{
    public class SearchController : BaseApiController
    {
        private readonly DataContext _context;
        public SearchController(DataContext context)
        {
            _context = context;
        }

        [HttpGet("{companyName}/{encodedUrl}")]
        public Site CallUrl(string companyName, string encodedUrl)
        {
            Site site = new Site();
            SiteServices _siteServices = new SiteServices();

            try
            {
                // Quand la persistence sera activé, ajouter un controle ici
                // Il faut vérifier que le Domaine n'est pas déja en BDD
                // Si oui on retourne le résultat de la BDD
                // Sinon on fait l'audit puis l'enregistrement

                byte[] base64EncodedBytes = Convert.FromBase64String(encodedUrl);
                site.SiteUrl = Encoding.UTF8.GetString(base64EncodedBytes);

                site.Domain = _siteServices.GetDomain(site.SiteUrl);

                site.Name = companyName;

                site = _siteServices.auditSite(site);

                return site;
            }
            catch (Exception er)
            {
                throw(er);
            }
        }

        [HttpGet("{searchTerms}")]
        public SearchResult googleSearch(string searchTerms)
        {
            SearchResult searchResult = new SearchResult();
            SiteServices _siteServices = new SiteServices();

            try
            {
                searchResult.searchTerms = searchTerms;

                _siteServices.googleSearch(searchResult);

                return searchResult;
            }
            catch (Exception er)
            {
 
                throw(er);
            }
        }
    }
}