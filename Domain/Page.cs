namespace Domain
{
    public class Page
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaCopyright { get; set; }
        public string OGMetaTitle { get; set; }
        public string OGMetaDescription { get; set; }
        public List<Link> Links { get; set; }

    }
}