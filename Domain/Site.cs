using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain
{
    public class Site
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid SiteId { get; set; }

        public string Name { get; set; }

        public string SiteUrl { get; set; }

        public string Domain { get; set; }

        public List<Link> SitemapLinks { get; set; }

        public List<Page> Pages { get; set; }

        public Guid? SocialMediaId { get; set; }

        public SocialMedia SocialMedia { get; set; }
    }
}