namespace Domain
{
    public class Result
    {
        public Guid Id { get; set; }
        public string SiteName { get; set; }
        public string SiteDescription { get; set; }
        public string SiteUrl { get; set; }
        public string LegalMentionUrl { get; set; }
        public List<string> PhoneNumbers { get; set; }
    }
}