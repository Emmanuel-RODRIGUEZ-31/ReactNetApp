namespace Domain
{
    public class Link
    {
        public Guid Id { get; set; }
        public string TypeFor { get; set; }
        public string Url { get; set; }
        public bool IsScrappedForSiteMap { get; set; }
    }
}