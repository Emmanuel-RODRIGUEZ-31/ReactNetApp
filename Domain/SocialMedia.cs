using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain
{
    public class SocialMedia
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid SocialMediaId { get; set; }

        public Guid? SiteId { get; set; }
        
        public Site Site { get; set; }

        public string Instagram { get; set; }

        public string Facebook { get; set; }

        public string Twitter { get; set; }

        public string Tiktok { get; set; }

        public string Linkedin { get; set; }

        public string Youtube { get; set; }

        public string Twitch { get; set; }
    }
}