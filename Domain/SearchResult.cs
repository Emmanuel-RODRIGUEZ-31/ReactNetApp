namespace Domain
{
    public class SearchResult
    {
        public Guid Id { get; set; }
        public string searchTerms { get; set; }
        public List<Result> Results { get; set; }
    }
}