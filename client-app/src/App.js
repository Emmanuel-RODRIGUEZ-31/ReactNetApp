import React, { useEffect, useState } from 'react';
import axios from "axios";
import './App.css';

const url = "https://www.jeuxvideo.com";
const companyName = "Jeux video.com";
const encoded = btoa(url);

const searchTerms = "mention solocal pamiers";

function App() {
  const [results, setResults] = useState([]);
  // const [googleSearchResult, setGoogleSearchResult] = useState([]);
  
  useEffect(() => {
    axios.get(`http://localhost:5000/api/Search/${companyName}/${encoded}`).then(response => {
      console.log(response.data);
      setResults(response.data);
    })
  }, [])

  // useEffect(() => {
  //   axios.get(`http://localhost:5000/api/Search/${searchTerms}`).then(response => {
  //     console.log(response.data);
  //     setGoogleSearchResult(response.data);
  //   })
  // }, [])

  return (
    <div className="App">
      <header className="App-header">
        <h1>WebGo Agency Prospect Application</h1>
        {/* <p>
          {results.domain}
        </p>
        <p>
          {results.name}
        </p>
        <p>
          {results.siteUrl}
        </p> */}
      </header>
    </div>
  );
}

export default App;
