namespace Application.Services
{
    public class ServicesBase
    {
        // Get Domain from an Url, without "www."
        public string GetDomain(string url)
        {
            try
            {
                string domain = new Uri(url).Host;
                string www = "www.";

                if (domain.StartsWith(www))
                {
                    domain = domain.Replace(www, "");
                }

                return domain;

            }
            catch (Exception er)
            {

                throw (er);
            }
        }

        public string RemoveSlash(string url) {
            string result = url.Trim();
            
            return result.EndsWith("/") ? result.Remove(result.Length - 1) : result;
        }

        public List<string> _socialMedia = new List<string>( new string[] {
            "instagram.com",
            "facebook.com",
            "twitter.com",
            "tiktok.com",
            "linkedin.com",
            "youtube.com",
            "twitch.tv"
        });

        public List<string> _extension = new List<string>(new string[] {
            ".jpeg",
            ".jpg",
            ".png",
            ".mov",
            ".mp4",
            ".avi",
            ".wmv",
            ".webm",
            ".flv",  
            ".html5",
            ".swf",
            ".f4v",
            ".xls",
            ".xlsx",
            ".doc",
            ".docx",
            ".pptx",
            ".exe",
        });

        public string _socialMediaType = "socialMedia";
        public string _sitemapType = "sitemap";
        public string _otherType = "other";
        public string _notSpecified = "Non renseigné ou non existant";
        public string _notFinded = "Non trouvé";
        public string _https = "https://";
        public string _www = "www.";
        public string _googleSearchUrl = "https://www.google.com/search?q=";
        public List<string> _legalMentionPossibility = new List<string>(new string[] {
            "mentions-legales",
            "Mention-Legales",
            "Mention-legales",
            "mentionslegales",
            "MentionsLegales",
            "mentionsLegales",
        });
        public List<string> _nodeTypes = new List<string>(new string[] {
            "//p",
            "//span",
            "//b",
            "//div",
            "//br",
        });
        public List<string> _phoneSectionStartWithPossibility = new List<string>(new string[] {
            "Téléphone",
            "téléphone",
            "Telephone",
            "telephone",
            "TÉLÉPHONE",
            "TELEPHONE",
            "Tél",
            "Tel",
            "TEL",
            "TÉL",
            "tel",
            "tél",
        });

        public List<string> _startSearchValues = new List<string>(new string[] {
            "&start=0",
            "&start=10",
        });
    }
}