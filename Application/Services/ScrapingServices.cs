using System.Collections.Specialized;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using Domain;
using HtmlAgilityPack;

namespace Application.Services
{
    public class ScrapingServices : ServicesBase
    {
        private HtmlWeb _web = new HtmlWeb();
        
        
        private HtmlDocument _htmlDoc = new HtmlDocument();

        #region Sitemap
        // Function how create a sitmap
        public List<Link> CreateSiteMap (List<Link> sitemap, string url)
        {
            try
            {
                // Call the function how create the first iteration of the sitemap   
                InitiateSitemap(sitemap, url);

                // Call recursive function du create a sitemap with all site links on all page of the sitemap
                MakeSitemap(sitemap);
              
                return sitemap;
            }
            catch (Exception er)
            {
                throw(er);
            }
        }

        // Add the first Link to the sitemap, with the given url
        // If the Url is not the home route of the site,
        // the funtion create the home route Link and add to the sitemap List
        private List<Link> InitiateSitemap(List<Link> sitemap, string url)
        {
            // Check if the Url is the home route of the site
            if (url.EndsWith(GetDomain(url)))
            {
                Link homeLink = new Link();
                // If true => create a Link and add it at sitemap List
                homeLink.TypeFor = "sitemap";
                homeLink.Url = RemoveSlash(url);
                homeLink.IsScrappedForSiteMap = false;
                sitemap.Add(homeLink);
            }
            else
            {
                // If false => create a Link with de home route of the site
                // and ad it to the sitemap List
                Link homeLink = new Link();
                string homeUrl = _https + _www + GetDomain(url);
                homeLink.TypeFor = "sitemap";
                homeLink.Url = RemoveSlash(homeUrl);
                homeLink.IsScrappedForSiteMap = false;
                sitemap.Add(homeLink);

                // and create another Link with de given url
                // and ad it to the sitemap List
                Link siteLink = new Link();
                siteLink.TypeFor = "sitemap";
                siteLink.Url = RemoveSlash(url);
                siteLink.IsScrappedForSiteMap = false;
                sitemap.Add(siteLink);
            }

            // Create a temp List to stock all Links find in
            List<Link> tempLinks = new List<Link>();

            // Create the first sitemap
            // Map each page in the sitemap
            foreach (Link page in sitemap)
            {
                Mapping(tempLinks, page);
                page.IsScrappedForSiteMap = true;
            }

            // Add all temp Links to the sitemap
            foreach (Link link in tempLinks)
            {
                sitemap.Add(link);
            }

            return sitemap;
        }

        // Recursive function how will build the sitemap
        private List<Link> MakeSitemap(List<Link> sitemap)
        {
            // Create a temp List to stock all Links find in
            List<Link> tempLinks = new List<Link>();

            // Create an int to count and stop iteration
            int count = 0;

            // Map each page in the sitemap
            foreach (Link page in sitemap)
            {
                // Check if the page was already scrapped
                if (page.IsScrappedForSiteMap == false)
                {
                    Mapping(tempLinks, page);
                    page.IsScrappedForSiteMap = true;
                }
                // If already scrapped, add one to count
                else
                {
                    count ++;
                }
            }

            // Check if all page of sitemap has beed scrapped
            // if true, it will end the function
            if (count != sitemap.Count())
            {
                // If false, add the new Links in site.
                foreach (Link link in tempLinks)
                {
                    // Add it only if so not exist already in sitemap
                    if (!sitemap.Exists(x => x.Url == link.Url))
                    {
                        sitemap.Add(link);
                    }
                }

                // And then recall the function
                MakeSitemap(sitemap);
            }

            return sitemap;
        }

        // Add all site links of a page in the Sitemap with <a href=""> find on the page
        private List<Link> Mapping(List<Link> tempLinks, Link pageUrl)
        {
            try
            {
                // Load page
                _htmlDoc = _web.Load(pageUrl.Url);

                // Search all <a> on the page
                HtmlNodeCollection aNodes = _htmlDoc.DocumentNode.SelectNodes("//a[@href]");

                // Scrap on link and look for sitemap links
                if (aNodes != null)
                {

                    foreach (HtmlNode node in aNodes)
                    {
                        // Initiate variables
                        string nodeValue = node.GetAttributeValue("href", string.Empty);
                        string rootUrl = _https + _www + GetDomain(pageUrl.Url);
                        Uri uri;
                        Regex urlRegex = new Regex(@"[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)");

                        // Check if the url is an intern url and if its not a download url or an url with an anchor (#)
                        if (nodeValue.StartsWith(rootUrl) && !_extension.Contains(Path.GetExtension(nodeValue.Trim())) && !nodeValue.Contains("#"))
                        {
                            // Create Uri
                            uri = new Uri(nodeValue);

                            // Check if the url contain a query string
                            // Don't need this kind of url
                            if (uri.Query != null && uri.Query.Count() == 0)
                            {
                                // Create a Link Object and fill it
                                Link l = new Link();
                                l.TypeFor = "sitemap";
                                l.Url = RemoveSlash(nodeValue);
                                l.IsScrappedForSiteMap = false;

                                // Create a variable to check if url contain a number
                                bool containsInt = l.Url.Any(char.IsDigit);

                                // Check if the end of the Url is only number (like: www.exemple.com/item-5485464)
                                // Don't need this kind of url
                                if (!containsInt)
                                {   
                                    // Check if the url do not exist in the temp List
                                    // Add it if it's true
                                    if (!tempLinks.Exists(x => x.Url == l.Url))
                                    {
                                        tempLinks.Add(l);
                                    }
                                }
                            }
                        }
                        // Check if the url not contain the domain part of url and start only after the fisrt slash
                        // and if its not a download url or an url with an anchor (#)
                        else if (!urlRegex.IsMatch(nodeValue) && !_extension.Contains(Path.GetExtension(nodeValue.Trim())) && !nodeValue.Contains("#") && nodeValue != "/")
                        {
                            // Create a clean url with the nodeValue
                            string baseUrl = _https + _www + GetDomain(pageUrl.Url);
                            
                           // Create a Link Object and fill it
                            Link l = new Link();
                            l.TypeFor = "sitemap";
                            l.Url = Char.IsLetter(nodeValue[0]) ? baseUrl + "/" + RemoveSlash(nodeValue) : baseUrl + RemoveSlash(nodeValue);
                            l.IsScrappedForSiteMap = false;

                            // Create Uri
                            uri = new Uri(l.Url);

                            // Check if the url contain a query string
                            // Don't need this kind of url
                            if (uri.Query != null && uri.Query.Count() == 0)
                            {   
                                // Create a variable to check if url contain a number
                                bool containsInt = l.Url.Any(char.IsDigit);

                                // Check if the end of the Url is only number (like: www.exemple.com/item-5485464)
                                // Don't need this kind of url
                                if (!containsInt)
                                {
                                    // Check if the url do not exist in the temp List
                                    // Add it if it's true
                                    if (!tempLinks.Exists(x => x.Url == l.Url))
                                    {
                                        tempLinks.Add(l);
                                    }
                                }
                            }
                        }
                    }
                }

                return tempLinks;

            }
            catch (Exception er)
            {
                throw (er);
            }
        }

        #endregion

        #region Goole Search
        public List<Result> GoogleSearch(string searchTerms)
        {
            List<Result> searchResults = new List<Result>();

            try
            {
                MakeSearch(searchResults, searchTerms);

                getLegalMentionInformations(searchResults);

                return searchResults;
            }
            catch (Exception er)
            {

                throw (er);
            }
        }

        private List<Result> MakeSearch(List<Result> searchResults, string searchTerms)
        {
            foreach (string value in _startSearchValues)
            {   

                // Load the result page with the search terms
                _htmlDoc = _web.Load(_googleSearchUrl + searchTerms + value);

                HtmlNodeCollection aNodes = _htmlDoc.DocumentNode.SelectNodes("//a[@href]");

                if (aNodes != null)
                {
                    foreach (HtmlNode node in aNodes)
                    {
                        Result result = new Result();

                        string nodeValue = node.GetAttributeValue("href", string.Empty);

                        if (nodeValue.StartsWith(_https + _www) || nodeValue.StartsWith(_https))
                        {

                            Uri uri = new Uri(nodeValue);
                            var query = HttpUtility.ParseQueryString(uri.Query);

                            if (uri.Query != null && uri.Query.Count() == 0)
                            {
                                foreach (string item in _legalMentionPossibility)
                                {
                                    if (nodeValue.EndsWith(item) || nodeValue.EndsWith(item + "/"))
                                    {
                                        result.LegalMentionUrl = nodeValue;
                                        result.SiteUrl = _https + _www + GetDomain(nodeValue);
                                        searchResults.Add(result);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            

            return searchResults;
        }

        // This function scrap a legal mention page and look for phone number
        private List<Result> getLegalMentionInformations(List<Result> searchResults)
        {
            Regex validatePhoneNumberRegex = new Regex(@"^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$");

            try
            {
                foreach (Result result in searchResults)
                {                    
                    // Load home site page to get his name and description
                    _htmlDoc = _web.Load(result.SiteUrl);

                    HtmlNodeCollection headNodes = _htmlDoc.DocumentNode.SelectNodes("//head");

                    foreach (var hnode in headNodes)
                    {
                        if (hnode.SelectSingleNode("//head/title") != null)
                        {
                            result.SiteName = Convert.ToString(hnode.SelectSingleNode("//head/title").InnerText);
                        }
                        else
                        {
                            result.SiteName = "Non trouvé";
                        }

                        if (hnode.SelectSingleNode("//meta[@name='description']") != null)
                        {
                            result.SiteDescription = Convert.ToString(hnode.SelectSingleNode("//meta[@name='description']").Attributes["content"].Value);
                        }
                        else
                        {
                            result.SiteDescription = "Non trouvé";
                        }
                    }

                    // Load legal mention page
                    _htmlDoc = _web.Load(result.LegalMentionUrl);

                    // Initiate a node collection
                    HtmlNodeCollection nodes;

                    // Initiate the phone number list
                    result.PhoneNumbers = new List<string>();

                    foreach (string nodeType in _nodeTypes)
                    {
                        nodes = _htmlDoc.DocumentNode.SelectNodes(nodeType);

                        if (nodes != null)
                        {
                            foreach (HtmlNode node in nodes)
                            {
                                string innerText;

                                if (nodeType == "//br" && node.NextSibling != null)
                                {
                                    innerText = node.NextSibling.InnerHtml.Trim();
                                }
                                else if (nodeType != "//br" && node.GetDirectInnerText() != null)
                                {
                                    innerText = node.GetDirectInnerText().Trim();
                                }
                                else
                                {
                                    innerText = "";
                                }

                                if (validatePhoneNumberRegex.IsMatch(innerText))
                                {
                                    if (!result.PhoneNumbers.Contains(innerText))
                                    {
                                        result.PhoneNumbers.Add(innerText); 
                                    }
                                }

                                foreach (string startWithPossibility in _phoneSectionStartWithPossibility)
                                {
                                    if (innerText.StartsWith(startWithPossibility))
                                    {
                                        string number = innerText.Remove(0, startWithPossibility.Length).Trim();

                                        if (validatePhoneNumberRegex.IsMatch(number))
                                        {
                                            if (!result.PhoneNumbers.Contains(innerText))
                                            {
                                                result.PhoneNumbers.Add(innerText);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (result.PhoneNumbers.Count() == 0)
                        {
                            result.PhoneNumbers.Add("Aucun numéro trouvé");
                        }
                    }
                }

                return searchResults;

            }
            catch (Exception er)
            {
                throw (er);
            }
        }
        // Scrap a page and get his Head informations and all his body Link (<a>)
        public List<Page> ScrapPage(List<Page> pages, string url)
        {
            Page page = new Page();

            Link link = new Link();

            try
            {
                _htmlDoc = _web.Load(url);

                // Call ScrapHeader function
                page = ScrapHeader(page, url);

                // Call ScrapBody function
                // page = ScrapBody(page, url);

                pages.Add(page);

                return pages;

            }
            catch (Exception er)
            {

                throw (er);
            }

        }

        private Page ScrapHeader(Page page, string url)
        {
            try
            {
                HtmlNodeCollection headNodes = _htmlDoc.DocumentNode.SelectNodes("//head");

                page.Title = Convert.ToString(_htmlDoc.DocumentNode.SelectSingleNode("//head/title").InnerHtml);

                page.Url = url;

                foreach (var node in headNodes)
                {
                    if (node.SelectSingleNode("//meta[@name='description']") != null)
                    {
                        page.MetaDescription = Convert.ToString(node.SelectSingleNode("//meta[@name='description']").Attributes["content"].Value);
                    }
                    else
                    {
                        page.MetaDescription = _notSpecified;
                    }
                    if (node.SelectSingleNode("//meta[@name='keywords']") != null && node.SelectSingleNode("//meta[@name='keywords']").Attributes["content"].Value != "")
                    {
                        page.MetaKeywords = Convert.ToString(node.SelectSingleNode("//meta[@name='keywords']").Attributes["content"].Value);
                    }
                    else
                    {
                        page.MetaKeywords = _notSpecified;
                    }
                    if (node.SelectSingleNode("//meta[@name='copyright']") != null)
                    {
                        page.MetaCopyright = Convert.ToString(node.SelectSingleNode("//meta[@name='copyright']").Attributes["content"].Value);
                    }
                    else
                    {
                        page.MetaCopyright = _notSpecified;
                    }
                    if (node.SelectSingleNode("//meta[@property='og:description']") != null)
                    {
                        page.OGMetaDescription = Convert.ToString(node.SelectSingleNode("//meta[@property='og:description']").Attributes["content"].Value);
                    }
                    else
                    {
                        page.OGMetaDescription = _notSpecified;
                    }
                    if (node.SelectSingleNode("//meta[@property='og:title']") != null)
                    {
                        page.OGMetaTitle = Convert.ToString(node.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].Value);
                    }
                    else
                    {
                        page.OGMetaTitle = _notSpecified;
                    }
                }

                return page;
            }
            catch (System.Exception)
            {

                throw;
            }

        }

        private Page ScrapBody(Page page, string url)
        {
            return page;
        }

        public SocialMedia GetSiteSocialMedia(SocialMedia socialMedia, string url)
        {
            try
            {
                // Load landing page a the site
                _htmlDoc = _web.Load(url);

                // Search all <a> on the page
                HtmlNodeCollection aNodes = _htmlDoc.DocumentNode.SelectNodes("//a[@href]");

                // Search for other social media link
                foreach (HtmlNode node in aNodes)
                {
                    string socialUrl = Convert.ToString(node.GetAttributeValue("href", string.Empty));

                    foreach (var media in _socialMedia)
                    {
                        string replaceUrl = "";

                        if (socialUrl.StartsWith(_https + _www + media))
                        {
                            replaceUrl = socialUrl.Replace(_https + _www, "");
                        }
                        else if (socialUrl.StartsWith(_https + media))
                        {
                            replaceUrl = socialUrl.Replace(_https, "");
                        }

                        if (media == replaceUrl)
                        {
                            switch (media)
                            {
                                case "instagram.com":
                                    socialMedia.Instagram = socialUrl;
                                    break;
                                case "facebook.com":
                                    socialMedia.Facebook = socialUrl;
                                    break;
                                case "twitter.com":
                                    socialMedia.Twitter = socialUrl;
                                    break;
                                case "tiktok.com":
                                    socialMedia.Tiktok = socialUrl;
                                    break;
                                case "linkedin.com":
                                    socialMedia.Linkedin = socialUrl;
                                    break;
                                case "youtube.com":
                                    socialMedia.Youtube = socialUrl;
                                    break;
                                case "twitch.tv":
                                    socialMedia.Twitch = socialUrl;
                                    break;

                                default:
                                    break;
                            }
                        }
                    }

                }
                return socialMedia;
            }
            catch (Exception er)
            {

                throw (er);
            }
        }

    #endregion
    }
}