using Domain;


namespace Application.Services
{
    public class SiteServices : ServicesBase
    {
        private ScrapingServices _scrapingServices = new ScrapingServices();

        // Main function of the application, call all Service she need to work;
        // May not contain logic;
        // Let logic for Services;
        public Site auditSite(Site site)
        {
            site.SitemapLinks = new List<Link>();
            site.Pages = new List<Page>();
            site.SocialMedia = new SocialMedia();

            try
            {
                site.SiteUrl = RemoveSlash(site.SiteUrl);

                site.SitemapLinks = _scrapingServices.CreateSiteMap(site.SitemapLinks, site.SiteUrl);

                // foreach (Link link in site.SitemapLinks)
                // {
                //     site.SocialMedia = _scrapingServices.GetSiteSocialMedia(site.SocialMedia, link.Url);

                //     site.Pages = _scrapingServices.ScrapPage(site.Pages, link.Url);
                // }

                return site;
            }
            catch (Exception er)
            {

                throw (er);
            }

        }

        public SearchResult googleSearch(SearchResult searchResult)
        {
            try
            {
                searchResult.Results = _scrapingServices.GoogleSearch(searchResult.searchTerms);

                return searchResult;
            }
            catch (Exception er)
            {
                
                throw(er);
            }
        }
    }
}