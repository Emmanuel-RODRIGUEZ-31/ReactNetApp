using Microsoft.EntityFrameworkCore;
using Domain;

namespace Persistence
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Site> Sites { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Link> Links { get; set; }
        public DbSet<SocialMedia> SocialMedias { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Site>()
                .HasMany(b => b.Pages)
                .WithOne();
            modelBuilder.Entity<Site>()
                .HasMany(b => b.SitemapLinks)
                .WithOne();
            modelBuilder.Entity<Site>()
                .HasOne(t => t.SocialMedia)
                .WithOne(t => t.Site)
                .HasForeignKey<SocialMedia>(t => t.SiteId);

            modelBuilder.Entity<SocialMedia>()
                .HasOne(t => t.Site)
                .WithOne(t => t.SocialMedia)
                .HasForeignKey<Site>(t => t.SocialMediaId);

            modelBuilder.Entity<Page>()
                .HasMany(b => b.Links)
                .WithOne();


            base.OnModelCreating(modelBuilder);
        }

    }

}